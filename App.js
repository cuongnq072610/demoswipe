/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, FlatList, Dimensions, Alert } from 'react-native';
import Swipeout from 'react-native-swipeout';

const data = [
    {
        id: 0,
        text: '0 HELLO WORLD'
    },
    {
        id: 1,
        text: '1 HELLO WORLD'
    },
    {
        id: 2,
        text: '2 HELLO WORLD'
    },
    {
        id: 3,
        text: '3 HELLO WORLD'
    },
    {
        id: 4,
        text: '4 HELLO WORLD'
    },
    {
        id: 5,
        text: '5 HELLO WORLD'
    },
]

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: data,
            rowId: -1,
        }
    }
    renderKey = (item) => {
        return item.id
    }

    onDelete = (index) => {
        const newData = this.state.data
        newData.splice(index, 1)
        this.setState({
            data: newData,
        })
    }

    renderItemFlatList = (item, index) => {
        const swipeSetting = {
            autoClose: true,
            close: !(index === this.state.rowId),
            onClose: () => { },
            onOpen: () => {
                this.setState({
                    rowId: index,
                })
            },
            right: [
                {
                    onPress: () => {
                        Alert.alert(
                            'Alert',
                            'Are you want to delete?',
                            [
                                {text: 'Yes', onPress: () => {
                                    this.onDelete(index)
                                }},
                                {text: 'No', onPress: () => { }},
                            ],
                            { cancelable: true }
                        )
                    },
                    text: 'Delete',
                    type: 'delete'
                }
            ],
            rowId: index,
            sectionId: 1
        }
        return (
            <Swipeout {...swipeSetting}>
                <View style={{ backgroundColor: 'green', padding: 10, marginVertical: 20, width: Dimensions.get("screen").width }}>
                    <Text style={{ color: 'white' }}> {item.text} </Text>
                </View>
            </Swipeout>
        )
    }

    render() {
        return (
            <View style={styles.container}>
                <FlatList
                    data={this.state.data}
                    renderItem={({ item, index }) => this.renderItemFlatList(item, index)}
                    keyExtractor={(item) => { this.renderKey(item) }}
                    extraData={this.state}
                >
                </FlatList>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
});
